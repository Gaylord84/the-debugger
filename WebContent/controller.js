/**
 * 
 */

//Constants declaration
const game = document.getElementById("game");
const canvasWidth = game.width = window.innerWidth;
const canvasHeight = game.height = window.innerHeight;
const context = game.getContext("2d");

const rotation = 360;

//Variables declaration
var rectangles = [];
var rectangle = null, selectRect = null;

var radian = (Math.PI/180), degree = 0;

var mousePosition = null, color = null;

var x= 0, y = 0, w = 0, h = 0;
var xStart = 0, yStart = 0;

var start = false, exist = false, progress = false;



/**
 * Function that chooses a color randomly
 * @returns color
 */
function randomColor() {

	var r = Math.floor(Math.random()*255);
	var g = Math.floor(Math.random()*255);
	var b = Math.floor(Math.random()*255);

	return color = 'rgb('+r+','+g+','+b+')';

};


/**
 * Function that checks if the mouse point is in an existing rectangle 
 * @returns
 */
function checkIsExisting() {

	rectangles.forEach(function(rectangle) {

		if((xStart >= rectangle.x && xStart <= (rectangle.x + rectangle.w)) || (xStart <= rectangle.x && xStart >= (rectangle.x + rectangle.w))) {

			if((yStart >= rectangle.y && yStart <= (rectangle.y + rectangle.h)) || (yStart <= rectangle.y && yStart >= (rectangle.y + rectangle.h))) {

				exist = true;
				selectRect = {id : rectangles.indexOf(rectangle), x:rectangle.x, y:rectangle.y, w:rectangle.w, h:rectangle.h, c : rectangle.c};

			}
		}
	});
};


/**
 * Action when the mouse click event is down
 */
game.addEventListener('mousedown', event => {

	//Starting position of the mouse
	mousePosition = game.getBoundingClientRect();
	xStart = Math.abs(event.clientX - mousePosition.left);
	yStart = Math.abs(event.clientY - mousePosition.top);

	checkIsExisting();

	//If is not existing
	if(!exist) {
		
		start = true;
		randomColor();
		
	}
});



/**
 * Function that draws a rectangle
 * @returns
 */
function drawRectangle() {

	if(start) {

		var xCurrent = 0, yCurrent = 0;

		//Clears all the context (canvas)
		context.clearRect(0,0,canvasWidth,canvasHeight);

		//Iteration the array [rectangles]
		rectangles.forEach(function (rectangle) {
			
			//Recovers the color of the rectangle selected
			context.strokeStyle = rectangle.c;
			//Draw the rectangle
			context.strokeRect(rectangle.x, rectangle.y, rectangle.w, rectangle.h);
			
		});

		//Current mouse position
		xCurrent = Math.floor(event.clientX - mousePosition.left);
		yCurrent = Math.floor(event.clientY - mousePosition.top);

		//Calculate the height and width of the rectangle
		w = Math.floor(xCurrent - xStart);
		h = Math.floor(yCurrent - yStart);

		context.strokeStyle = color;
		context.strokeRect(xStart, yStart, w, h);
		
	};
	
};


/**
 * Action when the mouse event move
 */
game.addEventListener('mousemove', event => {

	//If the rectangle exists and the drawing is started
	if(!exist && start) {

		//Call of the function drawRectangle()
		drawRectangle();

	};
});



/**
 * Function that adds a new rectangle in the array
 * @param xStart start point of the horizontal
 * @param yStart start point of the vertical
 * @param w width of the rectangle
 * @param h height of the rectangle
 * @param color color of the rectangle
 * @returns
 */
function addRectangle(xStart, yStart, w, h, color) {

	rectangle = {x : xStart, y : yStart, w : w, h : h, c : color};

	//Add "rectangle" in the array "rectangles"
	rectangles.push(rectangle);
	
};


/**
 * Function that clears the selected rectangle
 * @returns
 */
function deleteRectangle() {

	var timer;
	var i = 0;

	/**
	 * Function that calculates the center of selected rectangle
	 */
	function centerOfSelectedRectangle() {

		xCenter = selectRect.x + selectRect.w / 2;
		yCenter = selectRect.y + selectRect.h / 2;
		return center = {x : xCenter, y : yCenter};

	};

	/**
	 * Function that turns the selected rectangle
	 */
	function rotateRectangle() {	

		//Adding the value of the <degree> variable to the value of the radian variable
		degree += radian;
		
		//Save the initial context
		context.save();

		context.clearRect(0,0,canvasWidth,canvasHeight);

		//Increments the variable "centerOfRectangle"
		var centerOfRectangle = centerOfSelectedRectangle();
		
		//Change the center of the context to the center of the rectangle
		context.translate(centerOfRectangle.x, centerOfRectangle.y);
		
		//Rotate the <degree> of degree of the rectangle
		context.rotate(degree);
		
		context.strokeStyle = selectRect.c; 
		
		//Draws the rectangle selected after rotation
		context.strokeRect(-selectRect.w/2, -selectRect.h/2, selectRect.w, selectRect.h);

		//Restore the initial context
		context.restore();

		rectangles.forEach(function (rectangle) {
			if(rectangles.indexOf(rectangle) != selectRect.id) {
			
				context.strokeStyle = rectangle.c;
				context.strokeRect(rectangle.x, rectangle.y, rectangle.w, rectangle.h);
			
			}
		});

		i++;
		
		//Callback delay of the function rotateRrectangle()
		timer = setTimeout(rotateRectangle, 1);

		//If the rotation number is reached
		if(i >= rotation) {

			//Stop callback
			clearTimeout(timer);

			//Call of the function del()
			del();

		}
	};


	/**
	 * Function that clears the selected rectangle
	 */
	function del() {

		context.clearRect(0,0,canvasWidth,canvasHeight);

		//Clears the rectangle of the array [rectangles]
		rectangles.splice(selectRect.id, 1);

		rectangles.forEach(function (rectangle) {

			context.strokeStyle = rectangle.c;
			context.strokeRect(rectangle.x, rectangle.y, rectangle.w, rectangle.h);
			
			//Resets the value of the boolean variable
			exist = false; 

		});
	};

	//Call of the function rotateRectangle()
	rotateRectangle();

};


/**
 * Action when the mouse click event is up
 */
game.addEventListener('mouseup', event => {

	//If the rectangle exists
	if(!exist) {

		//Call the function addRectangle() with parameters
		addRectangle(xStart, yStart, w, h, color);
		
		//Resets values of different position variables
		xStart = 0, yStart = 0;
		x = 0, y = 0;
		w = 0, h = 0;
		
		start = false;

	} else {

		//Call of the function deleteRectangle();
		deleteRectangle();

	};
});